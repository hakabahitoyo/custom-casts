# ASUS

## しんまるこねむねむ

![](20190512031107.png)
![](20190512031240.png)
![](20190512031306.png)

### 悪魔風衣装

![](shinmaruko-nemunemu-devil-1.png)

## かわうちならは

![](20190512033649.png)
![](20190512211510.png)
![](20190512211755.png)
![](20190514204014.png)
![](20190514204124.png)
![](20190514204322.png)
![](20190514204356.png)
![](20190514204548.png)

### 悪魔風衣装

![](kawauchi-naraha-devil-1.png)
![](kawauchi-naraha-devil-2.png)
![](kawauchi-naraha-devil-3.png)
![](kawauchi-naraha-devil-4.png)

### アバター

![](naraha-avatar.png)

## インターネットの悪い存在

![](20190515003816.png)

### 眼鏡

![](yamahai-suehiro-glasses-1.png)
![](yamahai-suehiro-glasses-2.png)
![](yamahai-suehiro-glasses-3.png)

## あさがお

せる師みたいなもの。

![](asagao-1.png)
![](asagao-2.png)
![](asagao-3.png)
![](asagao-4.png)
![](asagao-5.png)

## 胡乱師

本人に会ったことないので想像。

![](uron-1.png)
![](uron-2.png)

## ひまわりプロドロモウ

黒人。アレクサンドラ・エルバキアンさんよりちょっと育った。

![](himawari-1.png)
![](himawari-2.png)
![](himawari-3.png)
![](himawari-4.png)

### アバター

![](himawari-avatar.png)